/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adri.pixelmon.Events;

import adri.pixelmon.Main;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;

/**
 *
 * @author adrian
 */
public class InventoryClick implements Listener{
    private Main plugin;
    
    public InventoryClick(Main Plugin) {
        this.plugin = Plugin;
        
    }
    @EventHandler
    public void clikInventory(InventoryClickEvent event){
        FileConfiguration config = plugin.getConfig();
        String pathInventory = ChatColor.translateAlternateColorCodes('&',config.getString("config.mochila.nombre"));
        String pathInventoryM = ChatColor.stripColor(pathInventory);
        if(ChatColor.stripColor(event.getInventory().getName()).equals(pathInventoryM)){
            if(event.getCurrentItem() == null){
                event.setCancelled(true);
                return;
            }
            if(event.getSlotType() == null){
                event.setCancelled(true);
                return;
            }else{
                Player jugador = (Player) event.getWhoClicked();
                event.setCancelled(true);
                if(event.getClickedInventory().equals(jugador.getOpenInventory().getTopInventory())){
                    if(event.getCurrentItem().hasItemMeta()){
                        Inventory inv = null;
                        switch(event.getSlot()){
                            
                            case 10:
                                inv  = Bukkit.createInventory(null, 54 ,"Mochila de los Pokemones");
                            break;
                            case 12:
                                inv  = Bukkit.createInventory(null, 54 ,"Mochila de Objetos");
                            break;
                            case 14:
                                inv  = Bukkit.createInventory(null, 54 ,"Tienda");
                            break;
                            case 16:
                                inv  = Bukkit.createInventory(null, 54 ,"Capturar un pokemon");
                            break;
                            

                        }
                        if(inv != null)jugador.openInventory(inv);
                          
                        
                    }
                    
                }
                
            }
            
        }
        
    }
    
}
