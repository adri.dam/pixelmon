/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adri.pixelmon;


import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.scheduler.BukkitScheduler;

/**
 *
 * @author adrian
 */
public class InventaryUpdate {
    private Main plugin;
    int taskID;
    
    public InventaryUpdate(Main plugin){
        this.plugin = plugin;
    }
      
    public void ejecutar(final Player jugador){
        BukkitScheduler sh = Bukkit.getServer().getScheduler();
        taskID = sh.scheduleSyncRepeatingTask(plugin,new Runnable(){
            public void run(){
                if(!update(jugador)){
                    Bukkit.getScheduler().cancelTask(taskID);
                    return;
                }
            }
        },0L,40L);
    }
    
    protected boolean update (Player jugador){
        FileConfiguration config = Main.plugin.getConfig();
        FileConfiguration pokedex = Main.plugin.getPokedex();
        String pathMenu = ChatColor.translateAlternateColorCodes('&',config.getString("config.mochila.nombre"));
        String pathMenuM = ChatColor.stripColor(pathMenu);
        String pathPokedex = ChatColor.translateAlternateColorCodes('&',config.getString("config.pokedex.nombre"));
        String pathPokedexM = ChatColor.stripColor(pathPokedex);
        Inventory inv = jugador.getOpenInventory().getTopInventory();
        String nameInv  = ChatColor.stripColor(inv.getName());
        if (inv != null){
            if(nameInv.equals(pathMenuM)){
                Inventary.Menu1(jugador, config);

                return true;
            }else if (nameInv.equals(pathPokedexM)){
                Inventary.pokedexMenu(jugador, config, pokedex);
             
                return true;
            }else{
                
                return false;
                
            }
        
            
        }
        return false;
        
    }

}
