package adri.pixelmon;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



import adri.pixelmon.Connected.ConnectionNode;
import adri.pixelmon.Events.InventoryClick;
import adri.pixelmon.commands.CommandPk;
import adri.pixelmon.commands.CommandPkdex;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

    
public class Main extends JavaPlugin{
    private ConnectionNode conn;
    public static Main plugin;
    private FileConfiguration messages;
    private File messagesFile;
    private FileConfiguration pokedex;
    private File pokedexFile;
    private FileConfiguration pokemons;
    private File pokemonsFile;
    private File config;
    
    public static void main(String[] args){}
    
    PluginDescriptionFile Plugin = getDescription();
    public String Version = ChatColor.RED + Plugin.getVersion();
    public String Name = ChatColor.YELLOW + "["+ Plugin.getName() + "]" + ChatColor.GRAY;
    public String rutaConfig;
    
    @Override
    public void onEnable(){
        plugin = this;
        conn = new ConnectionNode();
        registerConfig();
        FileConfiguration confiG = this.getConfig();
        final String urlBase = confiG.getString("config.urlServer");
        
        new Thread(new Runnable(){
            @Override
            public void run() {
                String msgOld = "";
                boolean servidorOnline = true;
                
                while(servidorOnline){
                    String msg = "";
                    
                    try {
                        try{
                            msg = conn.sendGet(urlBase + "/?get=prueba");
                            if( msg != msgOld){
                                Bukkit.getConsoleSender().sendMessage(msg);
                                getServer().broadcastMessage(msg);
                                msgOld = msg;

                            }
                        }catch(Exception ex){
                            servidorOnline = false;
                            Bukkit.getConsoleSender().sendMessage(Name + " Servidor no encontrado!");
                        }
                        
                        Thread.sleep(1000);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (Exception ex) {
                        Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                
                Bukkit.getConsoleSender().sendMessage(Name + " Conexion con el servidor terminada!");
            }
        }).start();
        Bukkit.getConsoleSender().sendMessage(Name +" Ha sido activado " +ChatColor.RED + "V" + Version);
        registerCommands();
        registerMessages();
        registerPokedex();
        registerEvents();
    }
    @Override
    public void onDisable(){
        Bukkit.getConsoleSender().sendMessage(Name +  " Ha sido desactivado");
    }
    
    public void registerCommands(){
        this.getCommand("pk").setExecutor(new CommandPk(this));
        this.getCommand("pkdex").setExecutor(new CommandPkdex(this));
    }
    public void registerEvents(){
        
       this.getServer().getPluginManager().registerEvents(new InventoryClick(this), this);
    }
    
    public void registerConfig(){
        config = new File(this.getDataFolder(),"config.yml");
        rutaConfig = config.getPath();
        if(!config.exists()){
            this.getConfig().options().copyDefaults(true);
            saveConfig();
        }
        plugin = this;
        
    }
    
    public FileConfiguration getMessages(){
        if(messages == null){
            reloadMessages();
        }
        plugin = this;
        return messages;
    }
   
    public void reloadMessages(){
        if(messages == null){
            messagesFile = new File(getDataFolder(),"messages.yml");
        }
        messages = YamlConfiguration.loadConfiguration(messagesFile);
        Reader defConfigStream;
        try{
            defConfigStream = new InputStreamReader(this.getResource("messages.yml"),"UTF8");
            if(defConfigStream != null){
                YamlConfiguration defConfig = YamlConfiguration.loadConfiguration(defConfigStream);
                messages.setDefaults(defConfig);
            }          
        }catch(UnsupportedEncodingException e){
            e.printStackTrace();
        }
        plugin = this;
        
    }
   
    public void saveMessages(){
        try{
            messages.save(messagesFile);           
        }catch(IOException e){
            e.printStackTrace();
        }
        plugin = this;
    }
   
    public void registerMessages(){
        messagesFile = new File(this.getDataFolder(),"messages.yml");
        if(!messagesFile.exists()){
            this.getMessages().options().copyDefaults(true);
            saveMessages();
        }
        
        plugin = this;
        
    }
    //###################################################################
    public FileConfiguration getPokedex(){
        if(pokedex == null){
            reloadPokedex();
        }
        plugin = this;
        return pokedex;
    }
   
    public void reloadPokedex(){
        if(pokedex == null){
            pokedexFile = new File(getDataFolder(),"pokedex.yml");
        }
        pokedex = YamlConfiguration.loadConfiguration(pokedexFile);
        Reader defConfigStream;
        try{
            defConfigStream = new InputStreamReader(this.getResource("pokedex.yml"),"UTF8");
            if(defConfigStream != null){
                YamlConfiguration defConfig = YamlConfiguration.loadConfiguration(defConfigStream);
                pokedex.setDefaults(defConfig);
            }          
        }catch(UnsupportedEncodingException e){
            e.printStackTrace();
        }
        plugin = this;
    }
   
    public void savePokedex(){
        try{
            pokedex.save(pokedexFile);           
        }catch(IOException e){
            e.printStackTrace();
        }
        plugin = this;
    }
   
    public void registerPokedex(){
        pokedexFile = new File(this.getDataFolder(),"pokedex.yml");
        if(!pokedexFile.exists()){
            this.getPokedex().options().copyDefaults(true);
            savePokedex();
        }
        plugin = this;
    }
    //###################################################################
    public FileConfiguration getPokemons(){
        if(pokemons == null){
            reloadPokemons();
        }
        plugin = this;
        return pokemons;
        
    }
   
    public void reloadPokemons(){
        if(pokedex == null){
            pokedexFile = new File(getDataFolder(),"pokemons.yml");
        }
        pokemons = YamlConfiguration.loadConfiguration(pokemonsFile);
        Reader defConfigStream;
        try{
            defConfigStream = new InputStreamReader(this.getResource("pokemons.yml"),"UTF8");
            if(defConfigStream != null){
                YamlConfiguration defConfig = YamlConfiguration.loadConfiguration(defConfigStream);
                pokemons.setDefaults(defConfig);
            }          
        }catch(UnsupportedEncodingException e){
            e.printStackTrace();
        }
        plugin = this;
    }
   
    public void savePokemons(){
        try{
            pokemons.save(pokemonsFile);           
        }catch(IOException e){
            e.printStackTrace();
        }
        plugin = this;
    }
   
    public void registerPomons(){
        pokedexFile = new File(this.getDataFolder(),"pokemons.yml");
        if(!pokemonsFile.exists()){
            this.getPokemons().options().copyDefaults(true);
            savePokemons();
        }
        plugin = this;
    }
    
}
