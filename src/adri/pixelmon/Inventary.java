/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adri.pixelmon;

import java.util.List;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

/**
 *
 * @author adrian
 */
public class Inventary {
    private static Inventory inv;
    
    public static void Menu1(Player jugador, FileConfiguration config){
        inv = jugador.getOpenInventory().getTopInventory();
        ItemStack item = new ItemStack(Material.getMaterial(config.getString("config.mochila.decoracion").toUpperCase()) ,1);
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(" ");
        item.setItemMeta(meta);
        for(int i = 0; i <= 8; i++){
            inv.setItem(i, item);
        }
        inv.setItem(9, item);
        inv.setItem(17, item);
        for(int i = 18; i <= 26; i++){
            inv.setItem(i, item);
        }
        
        
        ItemStack stack = configInventary(inv, config, new String[]{
            "config.mochila.pc.item",
            "config.mochila.pc.id",
            "config.mochila.pc.textura",
            "config.mochila.pc.nombre",
            "config.mochila.pc.lore"
        });
        inv.setItem(10, stack);
        
        stack = configInventary(inv, config, new String[]{
            "config.mochila.objetos.item",
            "config.mochila.objetos.id",
            "config.mochila.objetos.textura",
            "config.mochila.objetos.nombre",
            "config.mochila.objetos.lore"
        });
        inv.setItem(12, stack);
        
        stack = configInventary(inv, config, new String[]{
            "config.mochila.tienda.item",
            "config.mochila.tienda.id",
            "config.mochila.tienda.textura",
            "config.mochila.tienda.nombre",
            "config.mochila.tienda.lore"
        });
        inv.setItem(14, stack);
        
        stack = configInventary(inv, config, new String[]{
            "config.mochila.capturar.item",
            "config.mochila.capturar.id",
            "config.mochila.capturar.textura",
            "config.mochila.capturar.nombre",
            "config.mochila.capturar.lore"
        });
        inv.setItem(16, stack);
    }
    
    private static ItemStack configInventary(Inventory inv, FileConfiguration config, String[] urls){
        
        String material = config.getString(urls[0]).toUpperCase();
        ItemStack item = new ItemStack(Material.getMaterial(material) ,1);
        if(Material.PLAYER_HEAD.equals(Material.getMaterial(material))){
            item = Utilidades.getCabeza(item, config.getString(urls[1]), config.getString(urls[2]));
        }
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(Utilidades.translate(config.getString(urls[3])));
        List<String> lore = Utilidades.translate(config.getStringList(urls[4]));
        meta.setLore(lore);
        item.setItemMeta(meta);
        return item;
    }
    
    public static void pokedexMenu(Player jugador, FileConfiguration config, FileConfiguration pokedex){
        inv = jugador.getOpenInventory().getTopInventory();
        List<String> pokeNames = pokedex.getStringList("pokedexNames");
        for(int i = 0; i < pokeNames.size(); i++){
            String pokemon = pokeNames.get(i);
            ItemStack item = new ItemStack(Material.PLAYER_HEAD,1);
            item = Utilidades.getCabeza(item, pokedex.getString("pokemons." + pokemon + ".idTextur"), pokedex.getString("pokemons." + pokemon + ".textur"));
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName(Utilidades.translate(pokedex.getString("name.frontOf") 
                    + pokedex.getString("pokemons." + pokemon + ".name")
                    + pokedex.getString("name.dehind")));
            List<String> lore = Utilidades.translate(pokedex.getStringList("lore"), pokemon, pokedex);
            meta.setLore(lore);
            item.setItemMeta(meta);
            inv.setItem(i, item);
            
        }
        
    }
    
}
