/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adri.pixelmon.commands;

import adri.pixelmon.InventaryUpdate;
import adri.pixelmon.Main;
import adri.pixelmon.Utilidades;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

/**
 *
 * @author adrian
 */
public class CommandPkdex implements CommandExecutor{
    
    private Main plugin;
    private Player jugador;
    public CommandPkdex(Main Plugin) {
        this.plugin = Plugin;
        
    }
     
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        FileConfiguration messages = plugin.getMessages();
        FileConfiguration config = plugin.getConfig();
        
        /*if (args.length > 0){
            if (args[0].equalsIgnoreCase("reload")){
               //  /coor reload
               //plugin.reloadConfig();

                if(!(sender instanceof Player)){
                    Bukkit.getConsoleSender().sendMessage(messages.getString(plugin.Name + " El plugin ha sido cargado correctamente"));
                    return false;
                }

               jugador.sendMessage(plugin.Name + " El plugin ha sido cargado correctamente");

               return false;
            }else{*/
                if(!(sender instanceof Player)){
                    Bukkit.getConsoleSender().sendMessage(messages.getString("messages.consoleError"));
                    return false;
                }
                jugador = (Player)sender;
                Inventory inv = Bukkit.createInventory(null, 54 , Utilidades.translate(config.getString("config.pokedex.nombre")));
                jugador.openInventory(inv);
                InventaryUpdate iu = new InventaryUpdate(plugin);
                iu.ejecutar(jugador);
                /*return false;
            }
        }*/
        return true;
    }
    
    
}