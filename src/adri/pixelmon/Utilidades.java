/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adri.pixelmon;

import java.util.List;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.inventory.ItemStack;

/**
 *
 * @author adrian
 */
public class Utilidades {
    public static ItemStack getCabeza(ItemStack item, String id, String textura) {
        net.minecraft.server.v1_13_R2.ItemStack cabeza = org.bukkit.craftbukkit.v1_13_R2.inventory.CraftItemStack.asNMSCopy(item);
        net.minecraft.server.v1_13_R2.NBTTagCompound tag = cabeza.hasTag() ? cabeza.getTag() : new net.minecraft.server.v1_13_R2.NBTTagCompound();
        net.minecraft.server.v1_13_R2.NBTTagCompound skullOwnerCompound = new net.minecraft.server.v1_13_R2.NBTTagCompound();
        net.minecraft.server.v1_13_R2.NBTTagCompound propiedades = new net.minecraft.server.v1_13_R2.NBTTagCompound();
       
       
        net.minecraft.server.v1_13_R2.NBTTagList texturas = new net.minecraft.server.v1_13_R2.NBTTagList();
        net.minecraft.server.v1_13_R2.NBTTagCompound texturasObjeto = new net.minecraft.server.v1_13_R2.NBTTagCompound();
        texturasObjeto.setString("Value", textura);
        texturas.add(texturasObjeto);
        propiedades.set("textures", texturas);
        skullOwnerCompound.set("Properties", propiedades);
        skullOwnerCompound.setString("Id", id);
        tag.set("SkullOwner", skullOwnerCompound);
        cabeza.setTag(tag);
       
       
        return org.bukkit.craftbukkit.v1_13_R2.inventory.CraftItemStack.asBukkitCopy(cabeza);
    }
    
    public static String translate(String mensage){
       return ChatColor.translateAlternateColorCodes('&',mensage);
    }
    
    public static List<String> translate(List<String> lore){
        for (int i = 0; i< lore.size();i++){
            lore.set(i, ChatColor.translateAlternateColorCodes('&', lore.get(i)));
        }
       return lore;
    }
    public static List<String> translate(List<String> lore,String pokemon, FileConfiguration pokedex){
        for (int i = 0; i< lore.size();i++){
            lore.set(i, ChatColor.translateAlternateColorCodes('&', lore.get(i)));
            lore.set(i, lore.get(i).replaceAll("%type%", pokedex.getString("pokemons." + pokemon + ".type")));
            lore.set(i, lore.get(i).replaceAll("%ps%", pokedex.getString("pokemons." + pokemon + ".ps")));
            lore.set(i, lore.get(i).replaceAll("%att%", pokedex.getString("pokemons." + pokemon + ".attack")));
            lore.set(i, lore.get(i).replaceAll("%def%", pokedex.getString("pokemons." + pokemon + ".defense")));
            lore.set(i, lore.get(i).replaceAll("%vel%", pokedex.getString("pokemons." + pokemon + ".vel")));
        }
       return lore;
    }
}
